package com.duka.repalo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.duka.repalo.Model.listingModel
import com.duka.repalo.View.ListingCardView
import com.duka.repalo.View.NavigationBar
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.RepaloTheme
import com.duka.repalo.View.LoginView
import com.duka.repalo.View.RegisterView
import com.duka.repalo.View.listingView
import com.duka.repalo.ViewModel.ListingCardViewModel
import com.duka.repalo.ViewModel.ListingViewModel
import com.duka.repalo.ViewModel.LoginViewModel
import com.duka.repalo.ViewModel.RegisterViewModel
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.duka.repalo.View.CreateListingView
import com.duka.repalo.View.DisplayLocationMapView
import com.duka.repalo.View.MainScreenView
import com.duka.repalo.View.SelectLocationMapView
import com.duka.repalo.View.UserListingsView
import com.duka.repalo.ViewModel.CreateListingViewModel
import com.duka.repalo.ViewModel.MapViewModel
import com.duka.repalo.ViewModel.UserListingsViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            val navController = rememberNavController()
            RepaloTheme {
                NavHost(navController = navController, startDestination = "mainScreen") {
                    composable(
                        route = "mainScreen?listingNameSearch={listingNameSearch}",
                        arguments = listOf(navArgument("listingNameSearch") {
                            type = NavType.StringType
                            defaultValue = ""
                            nullable = true
                        })
                    ) { backStackEntry ->
                        val listingNameSearch = backStackEntry.arguments?.getString("listingNameSearch") ?: ""
                        MainScreenView(context = this@MainActivity, navController = navController, listingNameSearch = listingNameSearch)
                    }
                    composable(route = "userListings"){
                        UserListingsView(context = this@MainActivity, navController = navController, userListingsViewModel = UserListingsViewModel())
                    }
                    composable("loginScreen") {
                        LoginView(context = this@MainActivity, LoginViewModel(), navController = navController)
                    }
                    composable("registerScreen") {
                        RegisterView(context = this@MainActivity, registerViewModel = RegisterViewModel(), navController = navController)
                    }
                    composable(
                        route = "listingView/{listingID}",
                        arguments = listOf(navArgument("listingID") { type = NavType.StringType })
                    ) { backStackEntry ->
                        val listingID = backStackEntry.arguments?.getString("listingID") ?: "hSQTgINhGCBYUqse7Avc"
                        val listingViewModel = ListingViewModel() // Create your ListingViewModel instance
                        listingView(context = this@MainActivity, listingViewModel = listingViewModel, listingID = listingID, navController = navController)
                    }
                    composable("createListing"){
                        CreateListingView(createListingViewModel = CreateListingViewModel(), navController = navController)
                    }
                }
            }
        }
    }
}