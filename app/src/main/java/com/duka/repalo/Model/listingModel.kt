package com.duka.repalo.Model

data class listingModel(
    var name        : String?,
    var price       : Float?,
    var description : String?,
    var location    : String?,
    var seller      : String?,
    var contact     : String?,
    var category    : String?,
    var imageUrls   : List<String> = emptyList(),
    var id          : String? = null,
    var latitude    : Double? = null,
    var longitude   : Double? = null
)