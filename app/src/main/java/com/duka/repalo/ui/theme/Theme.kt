package com.duka.repalo.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext

// Existing color schemes
private val DarkColorScheme = darkColorScheme(
    primary = lightBlue,
    secondary = PurpleGrey80,
    tertiary = Pink80
)

private val LightColorScheme = lightColorScheme(
    primary = Purple40,
    secondary = PurpleGrey40,
    tertiary = Pink40
)

private val NavBarLightColorScheme = lightColorScheme(
    primary = Color(0xFF558DE0),
    onPrimary = white,
    primaryContainer = Color(0xFF558DE0),
    onPrimaryContainer = white,
    background = Color(0xFF558DE0),
    onBackground = white,
    surface = Color(0xFF558DE0),
    onSurface = white,
    secondary = Color(0xFFD9D9D9),
    onSecondary = Color.Black,
)

private val NavBarDarkColorScheme = darkColorScheme(
    primary = lightBlue,
    onPrimary = white,
    primaryContainer = lightBlue,
    onPrimaryContainer = white,
    background = lightBlue,
    onBackground = white,
    surface = lightBlue,
    onSurface = white,
    secondary = lightGrey,
    onSecondary = Color.Black,
)

private val ButtonColorScheme = lightColorScheme(
    primary = lightBlue,
    secondary = LightColorScheme.secondary,
    tertiary = LightColorScheme.tertiary
)

@Composable
fun RepaloTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> LightColorScheme
        else -> LightColorScheme
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}

@Composable
fun NavBarTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    dynamicColor: Boolean = false, // Assuming we don't need dynamic color for nav bar
    content: @Composable () -> Unit
) {
    val colorScheme = if (darkTheme) NavBarDarkColorScheme else NavBarLightColorScheme

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}

@Composable
fun RepaloButtonTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colorScheme = ButtonColorScheme,
        typography = Typography,
        content = content
    )
}