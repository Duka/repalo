package com.duka.repalo.ViewModel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.lifecycle.ViewModel
import com.duka.repalo.Model.listingModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await

class ListingContainerViewModel : ViewModel() {
    private val firestore = FirebaseFirestore.getInstance()
    private val listingsRef = firestore.collection("listings")

    suspend fun getAllListings(currentUser: String? = ""): List<listingModel> {
        return try {
            val query = if (currentUser.isNullOrBlank()) {
                listingsRef.get().await()
            } else {
                listingsRef.whereEqualTo("seller", currentUser).get().await()
            }

            query.documents.mapNotNull { document ->
                try {
                    val name = document.getString("name")
                    val price = document.getDouble("price")?.toFloat() // Assuming price is stored as Double in Firestore
                    val description = document.getString("description")
                    val location = document.getString("location")
                    val seller = document.getString("seller")
                    val contact = document.getString("contact")
                    val category = document.getString("category")

                    val imageUrlsMap = document.get("imageUrls") as? Map<String, Any> ?: emptyMap()
                    val imageUrls = imageUrlsMap.values.mapNotNull { it as? String }

                    listingModel(
                        name = name,
                        price = price,
                        description = description,
                        location = location,
                        seller = seller,
                        contact = contact,
                        category = category,
                        imageUrls = imageUrls,
                        id = document.id
                    )
                } catch (e: Exception) {
                    null
                }
            }
        } catch (e: Exception) {
            emptyList()
        }
    }
    fun resetFilter(listings: List<listingModel>, filteredListings: MutableState<List<listingModel>>) {
        filteredListings.value = listings.toList()
    }

    fun filterByName(listings: List<listingModel>, nameQuery: String): List<listingModel> {
        return if (nameQuery.isBlank()) {
            listings
        } else {
            listings.filter { it.name?.contains(nameQuery, ignoreCase = true) == true }
        }
    }


    fun filterByPrice(listings: List<listingModel>, minPrice: Float?, maxPrice: Float?): List<listingModel> {
        return listings.filter {
            val price = it.price
            (price != null && (minPrice == null || price >= minPrice) && (maxPrice == null || price <= maxPrice))
        }
    }


    fun filterByLocation(listings: List<listingModel>, locationQuery: String): List<listingModel> {
        return if (locationQuery.isBlank()) {
            listings
        } else {
            listings.filter { it.location?.contains(locationQuery, ignoreCase = true) == true }
        }
    }

    fun filterByCategory(listings: List<listingModel>, category: String): List<listingModel> {
        return if (category.isBlank()) {
            listings
        } else {
            listings.filter { it.category == category }
        }
    }


}