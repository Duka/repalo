package com.duka.repalo.ViewModel

import android.app.Activity
import android.net.Uri
import androidx.activity.result.ActivityResultLauncher
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class CreateListingViewModel : ViewModel() {
    val imageUris = mutableStateListOf<Uri>()
    val tempImageUri = mutableStateOf<Uri?>(null)

    fun takePicture(activity: Activity, cameraLauncher: ActivityResultLauncher<Uri>) {
        val photoFile = createImageFile(activity)
        val uri = FileProvider.getUriForFile(
            activity,
            "${activity.packageName}.provider",
            photoFile
        )
        tempImageUri.value = uri
        cameraLauncher.launch(uri)
    }

    fun selectImageFromGallery(galleryLauncher: ActivityResultLauncher<String>) {
        galleryLauncher.launch("image/*")
    }

    fun removeImage(uri: Uri) {
        imageUris.remove(uri)
    }

    private fun createImageFile(activity: Activity): File {
        val storageDir = activity.getExternalFilesDir(null)
        return File.createTempFile("JPEG_${System.currentTimeMillis()}", ".jpg", storageDir)
    }

    fun createListing(name: String, price: Float, description: String, location: String, contact: String, category: String, latitude: Double? = 0.0, longitude : Double? = 0.0) {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val listingsRef = FirebaseFirestore.getInstance().collection("listings")
        val storageRef = FirebaseStorage.getInstance().reference

        val listing = hashMapOf(
            "name" to name,
            "price" to price,
            "description" to description,
            "location" to location,
            "contact" to contact,
            "category" to category,
            "seller" to currentUserEmail,
            "latitude" to latitude,
            "longitude" to longitude
        )

        listingsRef
            .add(listing)
            .addOnSuccessListener { documentReference ->
                val documentId = documentReference.id // Get the newly created document ID
                val imageUrlsMap = mutableMapOf<String, String>()

                imageUris.forEachIndexed { index, uri ->
                    val imagePath = "listingImages/$documentId/image_$index.jpg"
                    val imageRef = storageRef.child(imagePath)

                    imageRef.putFile(uri)
                        .addOnSuccessListener {
                            imageUrlsMap["$index"] = imagePath

                            listingsRef.document(documentId)
                                .update("imageUrls", imageUrlsMap)
                                .addOnSuccessListener {
                                    println("Images uploaded and Firestore document updated successfully.")
                                }
                                .addOnFailureListener { e ->
                                    println("Error updating Firestore document: ${e.message}")
                                }
                        }
                        .addOnFailureListener { e ->
                            println("Error uploading image $index: ${e.message}")
                        }
                }
            }
            .addOnFailureListener { e ->
                println("Error creating Firestore document: ${e.message}")
            }
    }

}
