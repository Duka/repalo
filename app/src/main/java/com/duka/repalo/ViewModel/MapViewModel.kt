package com.duka.repalo.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MapViewModel : ViewModel() {
    private val _selectedLocation = MutableStateFlow<LatLng?>(null)
    val selectedLocation: StateFlow<LatLng?> get() = _selectedLocation
    private var currentPinLocation: LatLng? = null

    fun setLocation(location: LatLng) {
        viewModelScope.launch {
            _selectedLocation.emit(location)
        }
    }

    fun saveLocation(location: LatLng) {
        currentPinLocation = location
        viewModelScope.launch {
            _selectedLocation.emit(location)
        }
    }

    fun getCurrentPinLocation(): LatLng? {
        return currentPinLocation
    }
}
