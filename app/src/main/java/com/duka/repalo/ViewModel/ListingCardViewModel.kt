package com.duka.repalo.ViewModel

import androidx.lifecycle.ViewModel
import com.duka.repalo.Model.listingModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import kotlin.math.log

class ListingCardViewModel : ViewModel() {
    private val firestore = FirebaseFirestore.getInstance()
    private val listingsRef = firestore.collection("listings")

    suspend fun getListing(documentId: String): listingModel? {
        return try {
            val documentSnapshot = listingsRef.document(documentId).get().await()
            if (documentSnapshot.exists()) {
                val name = documentSnapshot.getString("name")
                val price = documentSnapshot.getDouble("price")?.toFloat() // Assuming price is stored as Double in Firestore
                val description = documentSnapshot.getString("description")
                val location = documentSnapshot.getString("location")
                val seller = documentSnapshot.getString("seller")
                val contact = documentSnapshot.getString("contact")
                val category = documentSnapshot.getString("category")

                val imageUrlsMap = documentSnapshot.get("imageUrls") as? Map<String, Any> ?: emptyMap()
                val imageUrls = imageUrlsMap.values.mapNotNull { it as? String }

                listingModel(name, price, description, location, seller, contact, category, imageUrls, documentId)
            } else {
                null
            }
        } catch (e: Exception) {
            println("Error getting document: ${e.message}")
            null
        }
    }

    suspend fun getAllListings(): List<listingModel> {
        return try {
            val querySnapshot = listingsRef.get().await()
            querySnapshot.documents.mapNotNull { document ->
                try {
                    val name = document.getString("name")
                    val price = document.getDouble("price")?.toFloat() // Assuming price is stored as Double in Firestore
                    val description = document.getString("description")
                    val location = document.getString("location")
                    val seller = document.getString("seller")
                    val contact = document.getString("contact")
                    val category = document.getString("category")

                    val imageUrlsMap = document.get("imageUrls") as? Map<String, Any> ?: emptyMap()
                    val imageUrls = imageUrlsMap.values.mapNotNull { it as? String }

                    listingModel(
                        name = name,
                        price = price,
                        description = description,
                        location = location,
                        seller = seller,
                        contact = contact,
                        category = category,
                        imageUrls = imageUrls,
                        id = document.id
                    )
                } catch (e: Exception) {
                    null
                }
            }
        } catch (e: Exception) {
            emptyList()
        }
    }
}