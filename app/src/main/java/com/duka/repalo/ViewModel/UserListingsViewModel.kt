package com.duka.repalo.ViewModel

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class UserListingsViewModel : ViewModel() {

    fun getCurrentUser() : String {
        val auth: FirebaseAuth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser

        if (currentUser != null) {
            return currentUser.email.toString()
        } else {
            return ""
        }
    }
}