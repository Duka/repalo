package com.duka.repalo.ViewModel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseAuth

class NavBarViewModel : ViewModel() {
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    fun logIn(){
        print("Logging in.")
    }
    fun register(){

    }
    fun logOut(context: Context, navController: NavController){

        auth.signOut()
        Toast.makeText(context, "Logged out successfully", Toast.LENGTH_SHORT).show()
        navController.navigate("mainScreen")
    }
}