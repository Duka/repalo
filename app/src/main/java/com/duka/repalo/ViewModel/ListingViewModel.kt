package com.duka.repalo.ViewModel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.duka.repalo.Model.listingModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.tasks.await

class ListingViewModel : ViewModel() {
    private val firestore = FirebaseFirestore.getInstance()
    private val listingsRef = firestore.collection("listings")

    suspend fun getListing(documentId: String): listingModel? {
        return try {
            val documentSnapshot = listingsRef.document(documentId).get().await()
            if (documentSnapshot.exists()) {
                val name = documentSnapshot.getString("name")
                val price = documentSnapshot.getDouble("price")?.toFloat() // Assuming price is stored as Double in Firestore
                val description = documentSnapshot.getString("description")
                val location = documentSnapshot.getString("location")
                val seller = documentSnapshot.getString("seller")
                val contact = documentSnapshot.getString("contact")
                val category = documentSnapshot.getString("category")
                val latitude = documentSnapshot.getDouble("latitude")
                val longitude = documentSnapshot.getDouble("longitude")

                val imageUrlsMap = documentSnapshot.get("imageUrls") as? Map<String, Any> ?: emptyMap()
                val imageUrls = imageUrlsMap.values.mapNotNull { it as? String }

                listingModel(
                    name = name,
                    price = price,
                    description = description,
                    location = location,
                    seller = seller,
                    contact = contact,
                    category = category,
                    imageUrls = imageUrls,
                    longitude = longitude,
                    latitude = latitude
                )
            } else {
                null
            }
        } catch (e: Exception) {
            println("Error getting document: ${e.message}")
            null
        }
    }

    suspend fun deleteListing(documentId: String, context: Context, navController: NavController) {
        val listingsRef = FirebaseFirestore.getInstance().collection("listings")
        val storageRef = FirebaseStorage.getInstance().reference

        try {
            val documentSnapshot = listingsRef.document(documentId).get().await()
            if (documentSnapshot.exists()) {
                val imageUrlsMap = documentSnapshot.get("imageUrls") as? Map<String, String>
                if (imageUrlsMap != null) {
                    imageUrlsMap.values.forEach { imagePath ->
                        val imageRef = storageRef.child(imagePath)
                        imageRef.delete().await()
                        println("Image deleted: $imagePath")
                    }
                }

                listingsRef.document(documentId).delete().await()
                println("Listing deleted successfully.")
                Toast.makeText(context, "Listing deleted", Toast.LENGTH_SHORT).show()
                navController.navigate("mainScreen")
            } else {
                Toast.makeText(context, "Listing with ID $documentId not found", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            Toast.makeText(context, "Error deleting listing: ${e.message}", Toast.LENGTH_SHORT).show()
            println("Error deleting listing: ${e.message}")
        }
    }

    fun updateListing(context : Context, documentId: String, price: Float, description: String, contact: String){
        val listingsRef = FirebaseFirestore.getInstance().collection("listings")
        val docRef = listingsRef .document(documentId)

        val updatedValues = mapOf(
            "price" to price,
            "description" to description,
            "contact" to contact,
        )

        docRef.update(updatedValues)
            .addOnSuccessListener {
                Toast.makeText(context, "Updated successfuly.", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{
                Toast.makeText(context, "Could not update listing.", Toast.LENGTH_SHORT).show()
            }
    }
}
