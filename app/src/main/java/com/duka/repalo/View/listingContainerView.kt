package com.duka.repalo.View

import android.content.Context
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.duka.repalo.Model.listingModel
import com.duka.repalo.ViewModel.ListingCardViewModel
import com.duka.repalo.ViewModel.ListingContainerViewModel
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.NavBarTheme
import com.duka.repalo.ui.theme.RepaloButtonTheme
import com.duka.repalo.ui.theme.RepaloTheme
import com.duka.repalo.ui.theme.lightBlue
import com.duka.repalo.ui.theme.white
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun listingContainerView(context: Context, navController: NavController, listingContainerViewModel: ListingContainerViewModel, listingNameSearch : String? = "", currentUser : String? = "") {
    val listingCardViewModel = ListingCardViewModel()
    val listings = remember { mutableStateOf<List<listingModel>>(emptyList()) }
    val filteredLisings = remember { mutableStateOf<List<listingModel>>(emptyList()) }

    var showMenu by remember { mutableStateOf(false) }
    var nameQuery by remember { mutableStateOf("") }
    var minPrice by remember { mutableStateOf<Float?>(null) }
    var maxPrice by remember { mutableStateOf<Float?>(null) }
    var locationQuery by remember { mutableStateOf("") }
    var categoryQuery by remember { mutableStateOf("") }
    val categories = remember { mutableStateListOf<String>() }
    var categoriesExpand by remember { mutableStateOf(false)}

    LaunchedEffect(Unit) {
        try {
            var allListings :  List<listingModel>
            if(currentUser != ""){
                allListings = listingContainerViewModel.getAllListings(currentUser = currentUser)
            }
            else{
                allListings = listingContainerViewModel.getAllListings()
            }
            listings.value = allListings
            if(listingNameSearch != ""){
                filteredLisings.value = listingContainerViewModel.filterByName(allListings, listingNameSearch!!)
            }
            else{
                filteredLisings.value = allListings
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    LaunchedEffect(Unit) {
            try {
                val db = FirebaseFirestore.getInstance()
                val documentSnapshot =
                    db.collection("listingCategories").document("categories").get().await()

                if (documentSnapshot.exists()) {
                    val fetchedCategories = mutableListOf<String>()
                    for (key in documentSnapshot.data?.keys ?: emptySet()) {
                        val category = documentSnapshot.getString(key)
                        if (category != null) {
                            fetchedCategories.add(category)
                        }
                    }
                    categories.addAll(fetchedCategories)
                    Log.d("Categories", "Added categories: $categories")
                } else {
                    Log.w("Categories", "Document 'categories' does not exist")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Categories", "Error fetching categories: ${e.message}", e)
            }
    }

    RepaloTheme {
        Column {
            Spacer(modifier = Modifier.height(32.dp))
            Box {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                ) {
                    if (currentUser != "") {
                        Text(
                            text = "Your listings",
                            style = TextStyle(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold
                            ),
                            modifier = Modifier.padding(start = 8.dp)
                        )
                    } else {
                        Text(
                            text = "Listings",
                            style = TextStyle(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold
                            ),
                            modifier = Modifier.padding(start = 8.dp)
                        )
                    }
                    RepaloButtonTheme{
                        Button(
                            onClick = { showMenu = !showMenu },
                            modifier = Modifier
                                .padding(start = 8.dp)
                                .background(
                                    color = lightBlue,
                                    shape = RoundedCornerShape(8.dp)
                                ) // Apply background color and rounded corners
                                .height(48.dp)
                                .padding(horizontal = 16.dp)
                        ) {
                            Text(
                                text = "Filters",
                                style = TextStyle(
                                    color = white,
                                    fontWeight = FontWeight.Bold
                                )
                            )
                        }

                    }
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                        .fillMaxHeight() // Ensures the Row takes full height to vertically center its content
                ) {
                    NavBarTheme {
                        DropdownMenu(
                            expanded = showMenu,
                            onDismissRequest = { showMenu = false },
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = lightBlue)
                                .padding(16.dp)
                                .align(Alignment.CenterVertically), // Aligns the DropdownMenu vertically center in the Row
                            content = {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally, // Centers content horizontally
                                    modifier = Modifier.fillMaxWidth()
                                ) {
                                    TextField(
                                        value = nameQuery,
                                        onValueChange = { nameQuery = it },
                                        label = { Text("Name") },
                                        textStyle = TextStyle(color = white)
                                    )
                                    Spacer(modifier = Modifier.height(8.dp))
                                    TextField(
                                        value = minPrice?.toString() ?: "",
                                        onValueChange = { minPrice = it.toFloatOrNull() },
                                        label = { Text("Min Price") },
                                        textStyle = TextStyle(color = white)
                                    )
                                    Spacer(modifier = Modifier.height(8.dp))
                                    TextField(
                                        value = maxPrice?.toString() ?: "",
                                        onValueChange = { maxPrice = it.toFloatOrNull() },
                                        label = { Text("Max Price") },
                                        textStyle = TextStyle(color = white)
                                    )

                                    Row(
                                        verticalAlignment = Alignment.CenterVertically
                                    ){
                                        Text("Category: ${categoryQuery}")
                                        IconButton(onClick = { categoriesExpand = !categoriesExpand }) {
                                            Icon(
                                                imageVector = Icons.Default.MoreVert,
                                                contentDescription = "More"
                                            )
                                        }
                                    }
                                    DropdownMenu(
                                        expanded = categoriesExpand,
                                        onDismissRequest  = {categoriesExpand = false }
                                    ){
                                        DropdownMenuItem(
                                            text = { Text("None") },
                                            onClick = {
                                                categoryQuery = ""
                                                categoriesExpand = false
                                            }
                                        )
                                        categories.forEach { category ->
                                            DropdownMenuItem(
                                                text = { Text(category) },
                                                onClick = {
                                                    categoryQuery = category
                                                    categoriesExpand = false
                                                }
                                            )
                                        }
                                    }
                                    Spacer(modifier = Modifier.height(8.dp))
                                    Button(
                                        onClick = {
                                            val filteredByName =
                                                listingContainerViewModel.filterByName(
                                                    listings.value,
                                                    nameQuery
                                                )
                                            val filteredByPrice =
                                                listingContainerViewModel.filterByPrice(
                                                    filteredByName,
                                                    minPrice,
                                                    maxPrice
                                                )
                                            val filteredByLocation =
                                                listingContainerViewModel.filterByLocation(
                                                    filteredByPrice,
                                                    locationQuery
                                                )
                                            val filteredByCategory =
                                                listingContainerViewModel.filterByCategory(
                                                    filteredByLocation,
                                                    categoryQuery
                                                )
                                            filteredLisings.value = filteredByCategory
                                            showMenu = false
                                        },
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .background(color = lightBlue)
                                    ) {
                                        Text("Apply Filters", fontWeight = FontWeight.Bold)
                                    }
                                }
                            }
                        )
                    }
                }


            }

            Spacer(modifier = Modifier.height(32.dp))

            Column {
                filteredLisings.value.forEach { listing ->
                    Spacer(modifier = Modifier.height(4.dp))
                    ListingCardView(
                        context = context,
                        listingCardViewModel = listingCardViewModel,
                        navController = navController,
                        listingID = listing.id ?: ""
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                }
            }
        }
    }
}
