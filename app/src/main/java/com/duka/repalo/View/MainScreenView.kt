package com.duka.repalo.View

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.duka.repalo.ViewModel.ListingContainerViewModel
import com.duka.repalo.ViewModel.MapViewModel
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.RepaloTheme

@Composable
fun MainScreenView(context : Context, navController : NavController, listingNameSearch : String? = "") {
    RepaloTheme {
        LazyColumn {
            item {
                Column {
                    NavigationBar(context = context, navBarViewModel = NavBarViewModel(), navController = navController)
                    listingContainerView(navController =  navController, context = context, listingContainerViewModel = ListingContainerViewModel(), listingNameSearch = listingNameSearch)
                    Spacer(modifier = Modifier.height(32.dp))
                }
            }
        }
    }
}