package com.duka.repalo.View

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.duka.repalo.ViewModel.MapViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState

@Composable
fun SelectLocationMapView(mapViewModel: MapViewModel) {
    val selectedLocation by mapViewModel.selectedLocation.collectAsState()

    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(LatLng(0.0, 0.0), 1f)
    }

    GoogleMap(
        modifier = Modifier
            .fillMaxWidth()
            .height(256.dp),
        cameraPositionState = cameraPositionState,
        onMapClick = { latLng ->
            mapViewModel.setLocation(latLng)
            mapViewModel.saveLocation(latLng)
        }
    ) {
        selectedLocation?.let {
            Marker(
                state = MarkerState(position = it),
                title = "Selected Location",
                snippet = "This is your chosen location"
            )
            cameraPositionState.move(CameraUpdateFactory.newLatLng(it))
        }
    }
}
