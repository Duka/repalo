package com.duka.repalo.View

import android.content.Context
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.duka.repalo.Model.listingModel
import com.duka.repalo.ViewModel.ListingCardViewModel
import com.duka.repalo.R
import com.duka.repalo.ui.theme.lightGrey
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.text.font.FontWeight
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun ListingCardView(context: Context, listingCardViewModel: ListingCardViewModel, navController: NavController, listingID: String) {
    val listingState = remember { mutableStateOf<listingModel?>(null) }
    val imageUrlsState = remember { mutableStateOf<List<String>>(emptyList()) }
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(listingID) {
        coroutineScope.launch {
            try {
                val listing = listingCardViewModel.getListing(listingID)
                if (listing != null) {
                    listingState.value = listing

                    listing.imageUrls?.map { imageUrl ->
                        Firebase.storage.reference.child(imageUrl).downloadUrl.await().toString()
                    }?.let { imageUrls ->
                        imageUrlsState.value = imageUrls
                    }
                } else {
                    Log.e("ListingViewModel", "Listing with ID $listingID not found")
                }
            } catch (e: Exception) {
                Log.e("ListingViewModel", "Error fetching listing: ${e.message}", e)
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(lightGrey, shape = RoundedCornerShape(8.dp))
            .clickable {
                navController.navigate("listingView/${listingID}")
            }
    )
    {
        Row(
            modifier = Modifier
                .padding(8.dp)
                .align(alignment = Alignment.CenterStart)
        ) {
            if (imageUrlsState.value.isNotEmpty()) {
                Image(
                    painter = rememberAsyncImagePainter(
                        model = ImageRequest.Builder(context)
                            .data(imageUrlsState.value[0])
                            .placeholder(R.drawable.notavailable)
                            .error(R.drawable.notavailable)
                            .build()
                    ),
                    contentDescription = listingState.value?.name ?: "Image",
                    modifier = Modifier
                        .size(100.dp)
                        .padding(8.dp)
                        .clip(shape = RoundedCornerShape(8.dp))
                )
            }
            Column(
                modifier = Modifier
                    .padding(start = 8.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            ) {
                listingState.value?.let { listing ->
                    Text(text = listing.name ?: "", fontWeight = FontWeight.Bold)
                    Text(text = "${listing.price}€")
                }
            }
        }
    }
}
