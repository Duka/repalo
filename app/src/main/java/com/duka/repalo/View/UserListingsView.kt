package com.duka.repalo.View

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.duka.repalo.ViewModel.ListingContainerViewModel
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ViewModel.UserListingsViewModel
import com.duka.repalo.ui.theme.RepaloTheme

@Composable
fun UserListingsView(context : Context, navController: NavController, userListingsViewModel : UserListingsViewModel){
    val currentUser = userListingsViewModel.getCurrentUser()

    RepaloTheme {
        LazyColumn {
            item {
                Column {
                    NavigationBar(context = context, navBarViewModel = NavBarViewModel(), navController = navController)
                    listingContainerView(navController =  navController, context = context, listingContainerViewModel = ListingContainerViewModel(), currentUser = currentUser)
                    Spacer(modifier = Modifier.height(32.dp))
                }
            }
        }
    }
}