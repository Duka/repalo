package com.duka.repalo.View


import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.duka.repalo.ViewModel.RegisterViewModel
import com.duka.repalo.ui.theme.NavBarTheme
import com.duka.repalo.ui.theme.lightGrey
import com.duka.repalo.ui.theme.white


@Composable
fun RegisterView(context: Context, registerViewModel: RegisterViewModel, navController : NavController){
    val username = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    val textStyle = TextStyle(fontSize = 20.sp, color = white)

    NavBarTheme {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color(0xFF558DE0)),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Email:",
                modifier = Modifier.padding(20.dp),
                style = textStyle
            )
            Box(
                modifier = Modifier
                    .background(lightGrey, shape = RoundedCornerShape(4.dp))
                    .padding(4.dp)
            ) {
                TextField(
                    value = username.value,
                    onValueChange = { newValue -> username.value = newValue },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                    modifier = Modifier.width(200.dp)
                )
            }
            Text(
                text = "Password:",
                modifier = Modifier.padding(20.dp),
                style = textStyle
            )
            Box(
                modifier = Modifier
                    .background(lightGrey, shape = RoundedCornerShape(4.dp))
                    .padding(4.dp)
            ) {
                TextField(
                    value = password.value,
                    onValueChange = { newValue -> password.value = newValue },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                    modifier = Modifier.width(200.dp),
                    visualTransformation = PasswordVisualTransformation()
                )
            }
            Row(
                modifier = Modifier.padding(20.dp)
            ) {
                Button(onClick = {
                    registerViewModel.register(context, username.value, password.value, navController)
                }) {
                    Text("Register", style = textStyle, fontWeight = FontWeight.Bold)
                }
                Button(onClick = {
                    navController.popBackStack()
                }) {
                    Text("Back", style = textStyle, fontWeight = FontWeight.Bold)
                }
            }
        }
    }
}