package com.duka.repalo.View

import android.content.Context
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.duka.repalo.Model.listingModel
import com.duka.repalo.R
import com.duka.repalo.ViewModel.ListingViewModel
import com.duka.repalo.ViewModel.MapViewModel
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.RepaloButtonTheme
import com.duka.repalo.ui.theme.RepaloTheme
import com.duka.repalo.ui.theme.lightBlue
import com.duka.repalo.ui.theme.white
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun listingView(context: Context, listingViewModel: ListingViewModel, listingID: String, navController: NavController) {
    val listingState = remember { mutableStateOf<listingModel?>(null) }
    val imageUrlsState = remember { mutableStateOf<List<String>>(emptyList()) }
    var isDropdownExpanded by remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()
    val mapViewModel = MapViewModel()
    var updatedDescription by remember { mutableStateOf(TextFieldValue()) }
    var updatedContact by remember { mutableStateOf(TextFieldValue()) }
    var updatedPrice by remember { mutableStateOf(0.0f) }

    LaunchedEffect(listingID) {
        coroutineScope.launch {
            try {
                val listing = listingViewModel.getListing(listingID)
                if (listing != null) {
                    listingState.value = listing

                    updatedPrice = listing.price ?: 0.0f
                    updatedContact = TextFieldValue(text = listing.contact ?: "")
                    updatedDescription = TextFieldValue(text = listing.description ?: "")

                    listing.imageUrls?.map { imageUrl ->
                        Firebase.storage.reference.child(imageUrl).downloadUrl.await().toString()
                    }?.let { imageUrls ->
                        imageUrlsState.value = imageUrls
                    }
                } else {
                    Log.e("ListingViewModel", "Listing with ID $listingID not found")
                }
            } catch (e: Exception) {
                Log.e("ListingViewModel", "Error fetching listing: ${e.message}", e)
            }
        }
    }

    LaunchedEffect(listingID) {
        coroutineScope.launch {
            try {
                val listing = listingViewModel.getListing(listingID)
                listingState.value = listing

                listing?.imageUrls?.map { imageUrl ->
                    Firebase.storage.reference.child(imageUrl).downloadUrl.await().toString()
                }?.let { imageUrls ->
                    imageUrlsState.value = imageUrls
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    listingState.value?.let { listing ->
        val textStyle = TextStyle(fontSize = 25.sp)

        RepaloTheme {
            LazyColumn {
                item {
                    NavigationBar(
                        context = context,
                        navBarViewModel = NavBarViewModel(),
                        navController = navController
                    )
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(color = white)
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth() // Fill the width of the parent to center the content horizontally
                                .padding(16.dp)
                        ) {
                            Text(
                                text = listing.name ?: "Default Name",
                                style = textStyle,
                                fontWeight = FontWeight.Bold
                            )
                            Text(
                                text = "${listing.price}€",
                                style = textStyle,
                                fontWeight = FontWeight.Bold
                            )

                            Spacer(modifier = Modifier.height(16.dp))

                            LazyRow(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                items(imageUrlsState.value) { uri ->
                                    Image(
                                        painter = rememberAsyncImagePainter(
                                            model = ImageRequest.Builder(LocalContext.current)
                                                .data(uri)
                                                .placeholder(R.drawable.notavailable)
                                                .error(R.drawable.notavailable)
                                                .build()
                                        ),
                                        contentDescription = "${listing.name ?: "Image"}",
                                        modifier = Modifier
                                            .size(300.dp)
                                            .padding(8.dp)
                                            .clip(shape = RoundedCornerShape(8.dp))
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(16.dp))

                            Text(text = listing.description ?: "Default description")

                            Spacer(modifier = Modifier.height(16.dp))

                            if(listing.longitude != null && listing.latitude != null) {
                                Text(
                                    text = buildAnnotatedString {
                                        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                            append("Location: ")
                                        }
                                        append(listing.location ?: "Unknown location")
                                    },
                                    style = textStyle
                                )
                                DisplayLocationMapView(
                                    mapViewModel = mapViewModel,
                                    longitude = listing.longitude ?: 0.0,
                                    latitude = listing.latitude ?: 0.0
                                )
                            }
                            Spacer(modifier = Modifier.height(16.dp))

                            Text(
                                text = buildAnnotatedString {
                                    withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                        append("Contact: ")
                                    }
                                    append(listing.contact ?: "Unknown seller")
                                },
                                style = textStyle
                            )

                            Text(
                                text = buildAnnotatedString {
                                    withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                        append("Seller: ")
                                    }
                                    append(listing.seller ?: "Unknown seller")
                                },
                                style = textStyle
                            )

                            Spacer(modifier = Modifier.height(16.dp))

                            val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email?.toString() ?: "notLoggedIn"
                            if(listing.seller == currentUserEmail) {

                                Button(
                                    onClick = {
                                        coroutineScope.launch {
                                            isDropdownExpanded = true
                                        }
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 32.dp),
                                    shape = RoundedCornerShape(4.dp),
                                    colors = ButtonDefaults.buttonColors(
                                        contentColor = white,
                                        containerColor = lightBlue
                                    )
                                ) {
                                    Text(
                                        text = "Edit Listing",
                                        style = textStyle,
                                        fontWeight = FontWeight.Bold,
                                        modifier = Modifier
                                    )
                                }

                                Spacer(modifier = Modifier.height(16.dp))

                                Button(
                                    onClick = {
                                        coroutineScope.launch {
                                            listingViewModel.deleteListing(
                                                documentId = listingID,
                                                context = context,
                                                navController = navController
                                            )
                                        }
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 32.dp),
                                    shape = RoundedCornerShape(4.dp),
                                    colors = ButtonDefaults.buttonColors(
                                        contentColor = white,
                                        containerColor = Color(LocalContext.current.getColor(R.color.button_red))
                                    )
                                ) {

                                    Text(
                                        text = "Delete Listing",
                                        style = textStyle,
                                        fontWeight = FontWeight.Bold,
                                        modifier = Modifier
                                    )
                                }
                            }
                            Spacer(modifier = Modifier.height(16.dp))
                        }
                        if (isDropdownExpanded) {
                            DropdownMenu(
                                expanded = isDropdownExpanded,
                                onDismissRequest = { isDropdownExpanded = false },
                                modifier = Modifier
                                    .padding(16.dp)
                                    .background(color = lightBlue)
                                    .fillMaxWidth()
                                    .align(Alignment.Center)
                                    .zIndex(1f)
                            ) {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    modifier = Modifier.fillMaxWidth()
                                        .background(color = lightBlue)
                                ) {

                                    Row(
                                        horizontalArrangement = Arrangement.End,
                                        modifier = Modifier.fillMaxWidth()
                                    )
                                    {
                                        IconButton(
                                            onClick = { isDropdownExpanded = false },
                                            modifier = Modifier
                                                .padding(end = 16.dp)
                                        ) {
                                            Icon(
                                                imageVector = Icons.Default.Close,
                                                contentDescription = "Close Dropdown",
                                                tint = white
                                            )
                                        }
                                    }

                                    Spacer(modifier = Modifier.height(16.dp))

                                    TextField(
                                        value = updatedDescription,
                                        onValueChange = { updatedDescription = it },
                                        label = { Text("Description") },
                                        textStyle = TextStyle(color = white)
                                    )

                                    Spacer(modifier = Modifier.height(8.dp))

                                    TextField(
                                        value = updatedContact,
                                        onValueChange = { updatedContact = it },
                                        label = { Text("Contact") },
                                        textStyle = TextStyle(color = white)
                                    )

                                    Spacer(modifier = Modifier.height(8.dp))

                                    TextField(
                                        value = updatedPrice?.toString() ?: "",
                                        onValueChange = { updatedPrice = it.toFloatOrNull() ?: 0.0f },
                                        label = { Text("Price") },
                                        textStyle = TextStyle(color = white)
                                    )

                                    Spacer(modifier = Modifier.height(8.dp))
                                    RepaloButtonTheme {
                                        Button(
                                            onClick = {
                                                listingViewModel.updateListing(
                                                    context = context,
                                                    documentId = listingID,
                                                    price = updatedPrice,
                                                    description = updatedDescription.text.toString(),
                                                    contact = updatedContact.text.toString(),
                                                )
                                                listing.price = updatedPrice
                                                listing.description = updatedDescription.text.toString()
                                                listing.contact = updatedContact.text.toString()
                                                isDropdownExpanded = false
                                            },
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .background(color = lightBlue)
                                        ) {
                                            Text(
                                                text = "Update listing",
                                                style = textStyle,
                                                fontWeight = FontWeight.Bold,
                                                modifier = Modifier
                                            )
                                        }
                                    }

                                    Spacer(modifier = Modifier.height(32.dp))
                                }
                            }
                        }
                    }
                }
            }

        }
    } ?: run {
        Text(text = "Loading...", modifier = Modifier.padding(16.dp))
    }
}
