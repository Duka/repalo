package com.duka.repalo.View

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.NavBarTheme
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import com.duka.repalo.ui.theme.lightGrey
import com.google.firebase.auth.FirebaseAuth

@Composable
fun NavigationBar(context: Context, navBarViewModel: NavBarViewModel, navController : NavController) {
    var search = remember { mutableStateOf("") }
    val textStyle = TextStyle(fontSize = 20.sp)
    val auth: FirebaseAuth = FirebaseAuth.getInstance()
    val currentUser = auth.currentUser

    NavBarTheme{
        Column(
            modifier = Modifier.fillMaxWidth()
                .background(Color(0xFF558DE0)),
            verticalArrangement = Arrangement.Top
        ){
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
                    .padding(top = 32.dp),
                horizontalArrangement = Arrangement.Start
            ) {
                if(currentUser != null){
                    Button(onClick = { navBarViewModel.logOut(context = context, navController = navController) }) {
                        Text("Log out", style = textStyle, fontWeight = FontWeight.Bold)
                    }
                    Spacer(modifier = Modifier.weight(1f))


                    Button(onClick = {
                        if (navController.currentDestination!!.route != "userListings") {
                            navController.navigate("userListings")
                        }
                        else{
                            navController.popBackStack()
                        }
                    })
                        {
                            if (navController.currentDestination!!.route != "userListings") {
                                Text("Your listings", style = textStyle, fontWeight = FontWeight.Bold)
                            }
                            else{
                                Text("Go back", style = textStyle, fontWeight = FontWeight.Bold)
                            }
                    }
                }
                else{
                    Button(onClick = { navController.navigate("loginScreen") }) {
                        Text("Log in", style = textStyle, fontWeight = FontWeight.Bold)
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Button(onClick = { navController.navigate("registerScreen") }) {
                        Text("Register", style = textStyle, fontWeight = FontWeight.Bold)
                    }

                }
            }

            Spacer(modifier = Modifier.height(48.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(
                    modifier = Modifier.background(lightGrey)
                ) {
                    TextField(
                        value = search.value,
                        onValueChange = { newValue -> search.value = newValue },
                        placeholder = { Text("Search...", style = textStyle) },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text, imeAction = ImeAction.Done),
                        keyboardActions = KeyboardActions(
                            onDone = {
                                navController.navigate("mainScreen?listingNameSearch=${search.value}")
                            }
                        ),
                        modifier = Modifier.width(200.dp)
                    )
                }
                Spacer(modifier = Modifier.weight(1f))
                if(currentUser != null) {
                    Button(onClick = { navController.navigate("createListing") }) {
                        Text("Post listing", style = textStyle, fontWeight = FontWeight.Bold)
                    }
                }
            }
        }
    }

}
