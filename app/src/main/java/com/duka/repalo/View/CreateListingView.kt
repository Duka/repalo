package com.duka.repalo.View

import android.app.Activity
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.duka.repalo.ViewModel.CreateListingViewModel
import com.duka.repalo.ViewModel.MapViewModel
import com.duka.repalo.ViewModel.NavBarViewModel
import com.duka.repalo.ui.theme.RepaloButtonTheme
import com.duka.repalo.ui.theme.RepaloTheme
import com.duka.repalo.ui.theme.lightBlue
import com.duka.repalo.ui.theme.white
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun CreateListingView(createListingViewModel: CreateListingViewModel, navController: NavController) {
    val context = LocalContext.current as Activity
    val coroutineScope = rememberCoroutineScope()

    val mapViewModel = MapViewModel()

    var name by remember { mutableStateOf("") }
    var contact by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }
    var location by remember { mutableStateOf("") }
    var price by remember { mutableStateOf(0.0f) }
    var selectedCategory by remember { mutableStateOf("") }
    val categories = remember { mutableStateListOf<String>() }
    val geoLocation = remember { mutableStateOf<LatLng?>(null)}
    val auth: FirebaseAuth = FirebaseAuth.getInstance()
    val currentUser = auth.currentUser

    var isDropdownExpanded by remember { mutableStateOf(false) }

    if (currentUser == null) {
        Log.d("Navigation", "User not logged in, navigating to loginScreen")
        navController.navigate("loginScreen")
    }

    val cameraLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) { success ->
            if (success) {
                createListingViewModel.tempImageUri.value?.let { uri ->
                    createListingViewModel.imageUris.add(uri)
                }
            }
        }

    val galleryLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let { createListingViewModel.imageUris.add(it) }
        }

    LaunchedEffect(Unit) {
        coroutineScope.launch {
            try {
                val db = FirebaseFirestore.getInstance()
                val documentSnapshot =
                    db.collection("listingCategories").document("categories").get().await()

                if (documentSnapshot.exists()) {
                    val fetchedCategories = mutableListOf<String>()
                    for (key in documentSnapshot.data?.keys ?: emptySet()) {
                        val category = documentSnapshot.getString(key)
                        if (category != null) {
                            fetchedCategories.add(category)
                        }
                    }
                    categories.addAll(fetchedCategories)
                    Log.d("Categories", "Added categories: $categories")
                } else {
                    Log.w("Categories", "Document 'categories' does not exist")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Categories", "Error fetching categories: ${e.message}", e)
            }
        }
    }

    RepaloTheme {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 64.dp) // Adjust bottom padding to accommodate map view if needed
            ) {
                item {
                    Column {
                        NavigationBar(
                            context = context,
                            navBarViewModel = NavBarViewModel(),
                            navController = navController
                        )
                        Spacer(modifier = Modifier.height(32.dp))

                        Column(modifier = Modifier.padding(16.dp)) {

                            Text("Create Listing", fontSize = 24.sp, fontWeight = FontWeight.Bold)

                            Spacer(modifier = Modifier.height(16.dp))

                            TextField(
                                value = name,
                                onValueChange = { name = it },
                                label = { Text("Name") },
                                modifier = Modifier.fillMaxWidth()
                            )

                            Spacer(modifier = Modifier.height(8.dp))

                            TextField(
                                value = contact,
                                onValueChange = { contact = it },
                                label = { Text("Contact") },
                                modifier = Modifier.fillMaxWidth()
                            )

                            Spacer(modifier = Modifier.height(8.dp))

                            TextField(
                                value = description,
                                onValueChange = { description = it },
                                label = { Text("Description") },
                                modifier = Modifier.fillMaxWidth()
                            )

                            Spacer(modifier = Modifier.height(8.dp))

                            TextField(
                                value = price.toString(),
                                onValueChange = {
                                    if (it.isNotEmpty()) {
                                        price = it.toFloat()
                                    } else {
                                        price = 0.0f
                                    }
                                },
                                label = { Text("Price") },
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                modifier = Modifier.fillMaxWidth()
                            )

                            Spacer(modifier = Modifier.height(8.dp))

                            Box(
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                RepaloButtonTheme {
                                    Button(
                                        onClick = { isDropdownExpanded = true },
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(48.dp)
                                            .clip(RoundedCornerShape(8.dp)) // Rounded corners
                                            .background(MaterialTheme.colorScheme.primary), // Blue background color
                                        contentPadding = PaddingValues(horizontal = 16.dp),
                                    ) {
                                        Text(
                                            "Select Location",
                                            fontWeight = FontWeight.Bold,
                                            color = Color.White
                                        )
                                    }
                                }

                                if (isDropdownExpanded) {
                                    IconButton(
                                        onClick = { isDropdownExpanded = false },
                                        modifier = Modifier
                                            .align(Alignment.CenterEnd)
                                            .padding(end = 16.dp)
                                    ) {
                                        Icon(
                                            imageVector = Icons.Default.Close,
                                            contentDescription = "Close Dropdown",
                                            tint = Color.Black
                                        )
                                    }
                                }
                            }

                            Spacer(modifier = Modifier.height(16.dp))

                            Text("Category", fontSize = 20.sp, fontWeight = FontWeight.Bold)

                            Spacer(modifier = Modifier.height(8.dp))

                            val rows = categories.chunked(3)
                            Column(
                                verticalArrangement = Arrangement.spacedBy(8.dp),
                                horizontalAlignment = Alignment.CenterHorizontally, // Center the column contents horizontally
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .wrapContentHeight()
                            ) {
                                rows.forEach { rowCategories ->
                                    Row(
                                        horizontalArrangement = Arrangement.SpaceEvenly, // Space the row contents evenly
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .wrapContentHeight()
                                    ) {
                                        rowCategories.forEach { category ->
                                            Box(
                                                modifier = Modifier
                                                    .wrapContentHeight()
                                                    .clip(RoundedCornerShape(8.dp))
                                                    .background(
                                                        if (selectedCategory == category) lightBlue else Color.Gray,
                                                        shape = RoundedCornerShape(8.dp)
                                                    )
                                                    .clickable { selectedCategory = category }
                                                    .padding(horizontal = 16.dp, vertical = 8.dp)
                                            ) {
                                                Text(
                                                    text = category,
                                                    color = Color.White,
                                                    modifier = Modifier.align(Alignment.Center)
                                                )
                                            }

                                        }
                                    }
                                }
                            }

                            Spacer(modifier = Modifier.height(16.dp))

                            RepaloButtonTheme {
                                Button(
                                    onClick = {
                                        createListingViewModel.takePicture(
                                            context,
                                            cameraLauncher
                                        )
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(48.dp)
                                        .clip(RoundedCornerShape(8.dp)) // Rounded corners
                                        .background(MaterialTheme.colorScheme.primary), // Blue background color
                                    contentPadding = PaddingValues(horizontal = 16.dp),
                                ) {
                                    Text(
                                        "Take Picture",
                                        fontWeight = FontWeight.Bold,
                                        color = Color.White
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(8.dp))

                            RepaloButtonTheme {
                                Button(
                                    onClick = {
                                        createListingViewModel.selectImageFromGallery(
                                            galleryLauncher
                                        )
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(48.dp)
                                        .clip(RoundedCornerShape(8.dp)) // Rounded corners
                                        .background(MaterialTheme.colorScheme.primary), // Blue background color
                                    contentPadding = PaddingValues(horizontal = 16.dp),
                                ) {
                                    Text(
                                        "Select Images from Gallery",
                                        fontWeight = FontWeight.Bold,
                                        color = Color.White
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(8.dp))

                            LazyRow {
                                items(createListingViewModel.imageUris.size) { index ->
                                    val uri = createListingViewModel.imageUris[index]
                                    Box(
                                        modifier = Modifier
                                            .padding(4.dp)
                                            .size(100.dp)
                                            .clip(RoundedCornerShape(8.dp))
                                            .background(Color.Gray)
                                    ) {
                                        Image(
                                            painter = rememberImagePainter(uri),
                                            contentDescription = null,
                                            modifier = Modifier
                                                .fillMaxSize()
                                                .clip(RoundedCornerShape(8.dp)),
                                            contentScale = ContentScale.Crop
                                        )
                                        Box(
                                            modifier = Modifier
                                                .size(24.dp)
                                                .clip(CircleShape)
                                                .background(Color.Red)
                                                .align(Alignment.TopEnd)
                                                .clickable {
                                                    createListingViewModel.removeImage(uri)
                                                }
                                        ) {
                                            Icon(
                                                imageVector = Icons.Default.Close,
                                                contentDescription = "Remove Image",
                                                tint = Color.White,
                                                modifier = Modifier.align(Alignment.Center)
                                            )
                                        }
                                    }
                                }
                            }

                            Spacer(modifier = Modifier.height(16.dp))

                            RepaloButtonTheme {
                                Button(
                                    onClick = {
                                        if(name != "" && description != "") {
                                            createListingViewModel.createListing(
                                                name = name,
                                                contact = contact,
                                                description = description,
                                                location = location,
                                                price = price,
                                                category = selectedCategory,
                                                longitude = geoLocation.value?.longitude,
                                                latitude = geoLocation.value?.latitude,
                                            )
                                            navController.popBackStack()
                                            Toast.makeText(
                                                context,
                                                "Listing posted",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                        else{
                                            if(name == "" && description == "") {
                                                Toast.makeText(
                                                    context,
                                                    "Name and description cannot be empty",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                            else if(name == "") {
                                                Toast.makeText(
                                                    context,
                                                    "Name cannot be empty",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                            else if(description == "") {
                                                Toast.makeText(
                                                    context,
                                                    "Description cannot be empty",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(48.dp)
                                        .clip(RoundedCornerShape(8.dp))
                                        .background(MaterialTheme.colorScheme.primary),
                                    contentPadding = PaddingValues(horizontal = 16.dp),
                                ) {
                                    Text(
                                        "Save Listing",
                                        fontWeight = FontWeight.Bold,
                                        color = Color.White
                                    )
                                }
                            }

                            Spacer(modifier = Modifier.height(24.dp))
                        }
                    }
                }
            }

            if (isDropdownExpanded) {
                Box(
                    modifier = Modifier
                        .padding(16.dp)
                        .background(lightBlue)
                        .align(Alignment.Center)
                ) {
                    Column(
                        horizontalAlignment = Alignment.End,
                        modifier = Modifier.fillMaxWidth()
                    ){
                        IconButton(
                            onClick = { isDropdownExpanded = false },
                            modifier = Modifier
                                .padding(end = 16.dp)
                        ) {
                            Icon(
                                imageVector = Icons.Default.Close,
                                contentDescription = "Close Dropdown",
                                tint = white
                            )
                        }
                        SelectLocationMapView(mapViewModel)
                        RepaloButtonTheme {
                            Button(
                                onClick = {
                                    geoLocation.value = mapViewModel.getCurrentPinLocation()
                                    if(geoLocation.value == null) {
                                        Toast.makeText(context, "Please place the pin.", Toast.LENGTH_LONG)
                                    }
                                    else{
                                        isDropdownExpanded = false
                                    }
                                },
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(48.dp)
                                    .clip(RoundedCornerShape(8.dp)) // Rounded corners
                                    .background(MaterialTheme.colorScheme.primary), // Blue background color
                                contentPadding = PaddingValues(horizontal = 16.dp),
                            ) {
                                Text(
                                    "Confirm location",
                                    fontWeight = FontWeight.Bold,
                                    color = Color.White
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
